package main.java.acme.game;

public class State {

    private int rewardRock=0;
    private int rewardPaper=0;
    private int rewardScissors=0;


    public int getRewardRock() {
        return rewardRock;
    }

    public void setRewardRock(int rewardRock) {
        this.rewardRock = rewardRock;
    }

    public int getRewardPaper() {
        return rewardPaper;
    }

    public void setRewardPaper(int rewardPaper) {
        this.rewardPaper = rewardPaper;
    }

    public int getRewardScissors() {
        return rewardScissors;
    }

    public void setRewardScissors(int rewardScissors) {
        this.rewardScissors = rewardScissors;
    }
}