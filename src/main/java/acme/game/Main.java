package main.java.acme.game;

import java.util.*;

public class Main {

    public static void main(String[] args){

        Game game=new Game();
        game.printTitle();
        String userAction;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.print("Your move: ");
            userAction=scanner.next();
            game.userMove(userAction);

        } while (!userAction.toLowerCase().trim().equals("q") && game.getTotalMoves()<20);
        game.printScores(true);
        System.out.println("Bye!");
    }
}
