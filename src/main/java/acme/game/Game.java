package main.java.acme.game;

import java.util.*;

public class Game {

    public static final int WIN_COMPUTER=1;
    public static final int WIN_USER=2;
    public static final int WIN_NOBODY=0;

    private static String[] moveTypes=new String[]{"r","p","s"};

    private  List<State> userRewardList=new ArrayList<State>();


    public  Map<String,String> actionNames=new HashMap<String,String>(){{
        put("r","Rock");
        put("p","Paper");
        put("s","Scissors");
    }};


    private int scoreComputer=0;
    private int scoreUser=0;
    private int totalMoves=0;

    private int lastMove=0;


    private boolean testing=false;



    public  Game(){

    }

    public int getScoreComputer(){
        return scoreComputer;
    }

    public int getScoreUser(){
        return scoreUser;
    }

    public  void setTesting(boolean t){
        testing=t;
    }


    public void printTitle(){
        System.out.println("Welcome to the game Rock, Paper, Scissors!\n\n" +
                "Your target is to make a move by choosing Rock, Paper or Scissors and get maximum points over computer within 20 turns.\n" +
                "Win moves are: \n" +
                "Rock -> Scissors\n" +
                "Scissors -> Paper\n" +
                "Paper -> Rock\n\n" +
                "Press (R) for Rock, (P) for Paper, (S) for Scissors, to quit game press (Q), to print score press (O)");
    }


    public boolean userMove(String userAction){
        if (userAction.trim().toLowerCase().equals("o")){
            printScores(false);
            return false;
        }
        ;
        if (!userAction.trim().toLowerCase().equals("q")){
            totalMoves++;
            String aiAction=makeMove();
            int res=checkResult(aiAction,userAction);
            switch (res){
                case WIN_COMPUTER:
                    if (!testing) System.out.println("Computer Won! "+actionNames.get(aiAction)+" -> "+actionNames.get(userAction));
                    break;
                case WIN_USER:
                    if (!testing) System.out.println("User Won! "+actionNames.get(userAction)+" -> "+actionNames.get(aiAction));
                    break;
                case WIN_NOBODY:
                    if (!testing) System.out.println("Nobody Won! "+actionNames.get(userAction)+" = "+actionNames.get(aiAction));
                    break;
                default:
                    return false;
            }
        }
        else {
            return true;
        }
        return false;
    }

    public void printScores(boolean gameFinished){
        if (gameFinished){
            if (scoreUser>scoreComputer){
                System.out.println("Congratulation! You Won!");
            }
            else if (scoreUser<scoreComputer){
                System.out.println("You lose! :(");
            }
            else {
                System.out.println("Game over! Nobody won.");
            }
        }
        System.out.println("\n--------------------------\n" +
                "\tyour score: "+scoreUser+"\n" +
                "\tcomputer score: "+scoreComputer+
                "\n--------------------------\n");
    }


    private int checkResult(String aiAction, String userAction) {
        int result=WIN_NOBODY;
        State state=new State();
        if (aiAction.equals("r") && userAction.equals("s")) {
            scoreComputer++;
            result= WIN_COMPUTER;
        }
        else if (aiAction.equals("s") && userAction.equals("r")) {
            scoreUser++;
            result= WIN_USER;
            state.setRewardScissors(1);
        }
        else if (aiAction.equals("r") && userAction.equals("p")) {
            scoreUser++;
            result= WIN_USER;
            state.setRewardRock(1);
        }
        else if (aiAction.equals("p") && userAction.equals("r")) {
            scoreComputer++;
            result= WIN_COMPUTER;
        }
        else if (aiAction.equals("s") && userAction.equals("p")) {
            scoreComputer++;
            result= WIN_COMPUTER;
        }
        else if (aiAction.equals("p") && userAction.equals("s")) {
            scoreUser++;
            result= WIN_USER;
            state.setRewardPaper(1);
        }
        if (result!=WIN_NOBODY && result!=WIN_COMPUTER){
            userRewardList.add(state);
        }
        return result;
    }

//userRewardList.size()<5 ? 0 : userRewardList.size()-5
    private  String makeMove() {

        int totalRock=0;
        int totalPaper=0;
        int totalScissors=0;
        int totalReward=0;
        for (int i=0;i<userRewardList.size();i++){
            State s=userRewardList.get(i);
            totalReward+=s.getRewardRock()+s.getRewardPaper()+s.getRewardScissors();
            totalRock+=s.getRewardRock();
            totalPaper+=s.getRewardPaper();
            totalScissors+=s.getRewardScissors();
        }

        // find move with lowest probability;
        int lowestIndex=-1;
        if (totalReward>0){
            double[] probabilities=new double[]{(double)totalRock/totalReward,(double)totalPaper/totalReward,(double)totalScissors/totalReward};

            double min=probabilities[0];

            for (int i=0;i<3;i++){
                if (probabilities[i]<min){
                    lowestIndex=i;
                    min=probabilities[i];
                }
            }
        }

        if (lowestIndex>=0){
            // make move if it not the same as previous one
            if (lowestIndex==lastMove){
                lowestIndex++;
                if (lowestIndex==3) lowestIndex=0;
            }
            lastMove=lowestIndex;
            return moveTypes[lowestIndex];
        }
        else {
            // just iterate to another kind of move
            lastMove++;
            if (lastMove==3) lastMove=0;
            return moveTypes[lastMove];
        }



    }

    public int getTotalMoves() {
        return totalMoves;
    }


}