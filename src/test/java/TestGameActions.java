package test.java;

import main.java.acme.game.Game;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestGameActions {

    @Test
    public void testRandomActions(){
        System.out.println("Test random actions...");
        Game game=new Game();
        String[] actions=getRandomActions();
        for (String userAction: actions){
            game.userMove(userAction);
        }
        assertEquals(actions.length,game.getTotalMoves());
        game.printScores(true);
    }

    @Test
    public void testSimilarAction(){
        System.out.println("Test similar actions...");
        List<String> result=new ArrayList<String>();
        for (int i=0;i<50;i++){
            Game game=new Game();
            game.setTesting(true);
            String[] actions=getSimilarActions();
            for (String userAction: actions){
                game.userMove(userAction);
            }
            assertEquals(actions.length,game.getTotalMoves());
            result.add("(Computer) "+game.getScoreComputer()+": "+game.getScoreUser()+" (User)");
        }
        System.out.println("result within 50 games with similar action: ");
        for (String s:result){
            System.out.println(s);
        }

    }



    private static String[] getSimilarActions() {
        Random rnd=new Random();
        int size=20;
        String[] vars=new String[]{"r","p","s"};
        int value=rnd.nextInt(3);
        String[] a=new String[size];
        for (int i=0;i<size;i++){
            a[i]=vars[value];
        }
        return a;
    }

    private static String[] getRandomActions() {
        Random rnd=new Random();
        int size=20;
        String[] vars=new String[]{"r","p","s"};
        String[] a=new String[size];
        for (int i=0;i<size;i++){
            a[i]=vars[rnd.nextInt(3)];
        }
        return a;
    }
}